
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const PORT = 3500;

app.use(express.json()) 
app.use (express.urlencoded({extended:true}));

const userRoutes = require('./routes/userRoutes');

mongoose.connect('mongodb+srv://seanmarkmira:qwerty12345@zuitt-bootcamp.1noa4.mongodb.net/course-booking?retryWrites=true&w=majority', 
	{useNewUrlParser: true, useUnifiedTopology: true}
);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log('Sucessfully connected to the database'));
app.use("/api/users", userRoutes);

app.listen(PORT, () => console.log('Server running at port 3500'));